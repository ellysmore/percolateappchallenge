![smallicon.png](https://bitbucket.org/repo/5n68Re/images/2581133005-smallicon.png)

Percolate Coffee
=======================

Summary: Android App that displays coffee post from Percolate Coffee API.

Instructions:
Compiled on *Andriod Studio*

Used RoboSpice OkHttp to make network calls and Eventbus to deliver results to screen.
I used picasso to load url into the image containers.

1/4/2015 UPDATES:
- Updated gradle to 1.0.
- Removed actionbar in favor of toolbar
- Minor UI tweaks